# mangadex-app

This project started because I wanted a manga reader to resemble the old version of MangaDex using V5 API. 
Due to CORS, I'm using Neutralino to make it an desktop application. 

> Login Support

> Probably doesn't work if the window width < 1280px

> This project is for personal use and I suck at UI (I'm sorry) 
## Dependencies

* [MangaDex API](https://api.mangadex.org/swagger.html)
* [React](https://github.com/facebook/react)
* [Tailwind](https://github.com/tailwindlabs/tailwindcss)
* [Neutralino](https://github.com/neutralinojs/neutralinojs)
* [Axios](https://github.com/axios/axios)
* [Luxon](https://github.com/moment/luxon)
* [React Router](https://github.com/ReactTraining/react-router)
* [React Hot Toast](https://github.com/timolins/react-hot-toast)
* [React Select](https://github.com/jedwatson/react-select)
* [Lipis Flag Icons](https://github.com/lipis/flag-icon-css)

## Testing

```bash
yarn start
```

## Building SPA

```bash
yarn build
```

## Building APP

```bash
nue build --releases
```
